package com.hra.helper;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.relevantcodes.extentreports.LogStatus;
 
public class JsonUtil{
	
	private JsonUtil(){
		
	}
	
	public static <T> void writeToFile(T data, String filePath) throws IOException {
		File outFile = new File(filePath);
		if (!outFile.exists()) {
			try {
				File directory = new File(outFile.getParent());
				if (!directory.exists()) {
					directory.mkdirs();
				}
				outFile.createNewFile();
			} catch (IOException e) {
				ReportingUtils.log(LogStatus.FAIL, "Exception Occurred while creating file: " + e.toString());
			}
		}
		
		try {
			ObjectMapper mapper = new ObjectMapper();

			// Java object to JSON file
			mapper.writeValue(outFile, data);
			
			ReportingUtils.log(LogStatus.PASS, "Json data saved at location: " + filePath + ". \nData: " + data);
 
		} catch (IOException e) {
			
			ReportingUtils.log(LogStatus.FAIL, "Error while saving Json data to file: " + e.toString());
			
		}
	}
 
	// Read From File Utility
	public static <T> T readFromFile(String filePath, Class<T> type) throws IOException {
		T obj = null;
		File inFile = new File(filePath);
		if (!inFile.exists()){
			ReportingUtils.log(LogStatus.FAIL, "File doesn't exist: " + filePath);
			Assert.fail("File doesn't exist: " + filePath);
		}
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			//JSON file to Java object
			obj = mapper.readValue(inFile, type);
			
		} catch (IOException e) {
			
			ReportingUtils.log(LogStatus.FAIL, "Data loaded failed in Object from file: " + e.toString());
			
		}
		
		ReportingUtils.log(LogStatus.PASS, "Data loaded successfully in Object from file: " + filePath);
 
		return obj;
 
	}
	
	public static <T> String objToJsonStr(T data){
		String jsonStr = "";
		ObjectMapper obj = new ObjectMapper();

		try {
			jsonStr = obj.writeValueAsString(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return jsonStr;
	}
 
}
