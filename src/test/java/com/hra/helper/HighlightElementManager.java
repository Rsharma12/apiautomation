package com.hra.helper;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HighlightElementManager {
	public static void highlightElement(WebDriver driver, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;

		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

			System.out.println(e.getMessage());
		}

	}
	
	public static void unHighlightElement(WebDriver driver, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;

		js.executeScript("arguments[0].setAttribute('style','');", element);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

			System.out.println(e.getMessage());
		}

	}
}
