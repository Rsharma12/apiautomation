package com.hra.testrunner;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.hra.helper.ReportingUtils;
import com.relevantcodes.extentreports.ExtentReports;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;


@CucumberOptions(
					features = "src/test/resources/FeatureFiles/UpdatedTicketAPIs.feature",
//					features = "src/test/resources/FeatureFiles/CreateTicketAPIs.feature",
//		features = "src/test/resources/FeatureFiles/CreateTicketAPIs.feature",
					glue = {"com/hra/api/stepdef"}, 
					plugin = { "pretty","html:target/cucumber-htmlreport", "json:target/cucumber-report.json", "com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html" },
//					tags = {"@RbacRoles"},
					monochrome = true)
public class TestRunner {

	private TestNGCucumberRunner testNGCucumberRunner;

	@BeforeClass(alwaysRun = true)
	public void setUpClass() {
		testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
		ReportingUtils.report = new ExtentReports("C:\\ExtentReport" + "\\ExtentReportResults.html");
//		TestBase testBase = new TestBase();
//		testBase.selectBrowser(Browsers.CHROME.name());
	}

	@Test(groups = "cucumber", description = "Runs cucumber Features", dataProvider = "features")
	public void feature(CucumberFeatureWrapper cucumberFeature) {
		testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
	}

	@DataProvider
	public Object[][] features() {
		return testNGCucumberRunner.provideFeatures();
	}

	@AfterClass(alwaysRun = true)
	public void testDownClass() {
		
		ReportingUtils.report.flush();
		testNGCucumberRunner.finish();
//		TestBase.driver.quit();
	}

}
