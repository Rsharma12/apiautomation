package com.hra.api.token;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.junit.Assert;

import static com.hra.api.config.Config.*;

import com.hra.api.pojo.TokenResponse;
import com.hra.helper.ReportingUtils;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.relevantcodes.extentreports.LogStatus;

import static com.jayway.restassured.RestAssured.*;

public class TokenId {
	
	private TokenId(){
		
	}
	
	public static String tokenId;
	public static Date expiryDate;
	
	public static void getToken() throws ParseException, IOException{
		String tokenReqUrl = BASE_URL + "Token";
		
		RequestSpecBuilder builder = new RequestSpecBuilder();
		builder.addParams(PARAM_MAP);
		builder.setContentType(APP_URLENCODED);
				
		RequestSpecification requestSpec = builder.build();
		
		Response response = given().spec(requestSpec).when().post(tokenReqUrl);
		
		if(response.statusCode() == 200){
			TokenResponse tokenResponse = response.as(TokenResponse.class);
			tokenId = tokenResponse.getAccessToken();
			
			SimpleDateFormat inputFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'", Locale.US);
			inputFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
			expiryDate = inputFormat.parse(tokenResponse.getExpires());
			String message = response.asString();
			ReportingUtils.log(LogStatus.PASS, message);
			
		}else{
			ReportingUtils.log(LogStatus.FAIL, response.asString());
			Assert.fail(response.asString());
		}
		
	}
	
	public static boolean isValid(){
		Date now = new Date();
		long diffInSeconds = (expiryDate.getTime() - now.getTime())/1000;
	    if(diffInSeconds < 5){
	    	return false;
	    }else{
	    	return true;
	    }
		
	}

}
