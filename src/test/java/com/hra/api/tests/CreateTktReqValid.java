package com.hra.api.tests;

import static com.hra.api.config.Config.*;
import static com.jayway.restassured.RestAssured.given;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hra.api.config.Config;
import com.hra.api.pojo.AddlInfo;
import com.hra.api.pojo.Case;
import com.hra.api.pojo.CaseResponse;
import com.hra.api.pojo.TicketResponse;
import com.hra.api.pojo.CreateTktReqData;
import com.hra.api.pojo.GetTktIdByRefIdResponse;
import com.hra.api.pojo.GetTktIdByRefIdStatus;
import com.hra.api.pojo.InvolvedParty;
import com.hra.api.pojo.Issue;
import com.hra.api.pojo.TicketData;
import com.hra.api.pojo.TktIdByRefIdReqData;
import com.hra.api.pojo.TokenResponse;
import com.hra.api.token.TokenId;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

public class CreateTktReqValid {
	
	/*private String referenceId = "";
	private String token = "";

	@Test
	public void createTktReqValid() throws ParseException {

		String jsonStr = "";
		CreateTktReqData createTktReqData = createTktData();

		ObjectMapper Obj = new ObjectMapper();

		try {

			// get Oraganisation object as a json string
			jsonStr = Obj.writeValueAsString(createTktReqData);

			// Displaying JSON String
			System.out.println(jsonStr);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String createTktReqUrl = BASE_URL + "CreateTicket";
				
		TokenResponse tokenResponse = TokenId.getToken();
		token = tokenResponse.getAccessToken();
		System.out.println(token);
		System.out.println(createTktReqUrl);
		Response response = given().authentication().oauth2(token).contentType("application/json; charset=UTF-8").body(jsonStr)
					.when().post(createTktReqUrl);
		
		System.out.println(response.asString());
		CreateTicketResponse createTicketResponse = response.as(CreateTicketResponse.class);
		System.out.println("Status = " + createTicketResponse.getStatus());
		System.out.println("Reference ID = " + createTicketResponse.getData().getReferenceId());
		referenceId = createTicketResponse.getData().getReferenceId();

		

		System.out.println(tokenResponse.getAccessToken());
		System.out.println(tokenResponse.getExpires());
		String expiryTime = tokenResponse.getExpires();
		SimpleDateFormat inputFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'", Locale.US);
		inputFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
		SimpleDateFormat out = new SimpleDateFormat("MMM dd, yyyy h:mm a");
		Date tokenExpiryTime = inputFormat.parse(expiryTime);
		
		String output = out.format(tokenExpiryTime);
		System.out.println(output);
		
		Date now = new Date();
		System.out.println("Current Time - " + now);
		System.out.println("Current Time formatted - " + out.format(now));
		System.out.println(tokenExpiryTime.after(now));
		
		long diffInSeconds = (tokenExpiryTime.getTime() - now.getTime())/1000;
	    System.out.println(diffInSeconds);
		
		

	}
	
	@Test
	public void getTktIdByRefId() throws InterruptedException{
		String jsonStr = "";
		TktIdByRefIdReqData tktIdByRefIdReqData = new TktIdByRefIdReqData(referenceId);

		ObjectMapper Obj = new ObjectMapper();

		try {

			// get Oraganisation object as a json string
			jsonStr = Obj.writeValueAsString(tktIdByRefIdReqData);

			// Displaying JSON String
			System.out.println(jsonStr);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String getTicketIdByRefIdUrl = BASE_URL + "GetTicketIdByRefId";
				
		TokenResponse tokenResponse = TokenId.getToken();
		token = tokenResponse.getAccessToken();
		System.out.println(token);
		System.out.println(createTktReqUrl);
		Response response = given().authentication().oauth2(token).contentType("application/json; charset=UTF-8").body(jsonStr)
					.when().post(getTicketIdByRefIdUrl);
		
		System.out.println(response.asString());
		while(response.asString().contains("is in progress")){
			TimeUnit.SECONDS.sleep(10);
			TokenResponse tokenResponse = TokenId.getToken();
			token = tokenResponse.getAccessToken();
			response = given().authentication().oauth2(token).contentType("application/json; charset=UTF-8").body(jsonStr)
						.when().post(getTicketIdByRefIdUrl);
			System.out.println("Retrying ....");
		}
		
		GetTktIdByRefIdResponse getTktIdByRefIdResponse = response.as(GetTktIdByRefIdResponse.class);
		
		for(GetTktIdByRefIdStatus status : getTktIdByRefIdResponse.getData()){
			System.out.println("Status:" + status.getStatus());
			System.out.println("Source Ticket ID: " + status.getSrcTicketId());
			System.out.println("Ticket ID: " + status.getTicketNo());
		}
		
		System.out.println(response.asString());
//		CreateTicketResponse createTicketResponse = response.as(CreateTicketResponse.class);
		
	}

	private CreateTktReqData createTktData() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
		DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("HHmmss");
		String time = now.format(dtf1);
		String currentTime = now.format(dtf);
		System.out.println(currentTime);
		Random r = new Random();

		AddlInfo addlInfo = new AddlInfo("Additional Info", "Additional info at time - " + currentTime);
		List<AddlInfo> addlInfoList = new ArrayList<>();
		addlInfoList.add(addlInfo);

		int rand = r.nextInt(issueCategories.length);
		Issue issue = new Issue(issueCategories[rand], issueSubCategories[rand],
				"Issue description at time - " + currentTime);
		List<Issue> issueList = new ArrayList<>();
		issueList.add(issue);

		rand = r.nextInt(invPartyRoles.length);
		InvolvedParty involvedParty = new InvolvedParty("Test FN", "Test LN", "EID-1234", invPartyRoles[rand]);
		List<InvolvedParty> involvedPartyList = new ArrayList<>();
		involvedPartyList.add(involvedParty);

		String srcTicketId = "TKT-AUTO-API-" + time;
		String caseName = "Case-" + time;
		rand = r.nextInt(groups.length);
		String group = groups[rand];

		rand = r.nextInt(locations.length);
		String location = locations[rand];

		rand = r.nextInt(notificationMethods.length);
		String notificationMethod = notificationMethods[rand];

		String issueDetail = "Ticket created using Automated API at - " + currentTime;

		TicketData ticketData = new TicketData(srcTicketId, caseName, group, location, notificationMethod, issueDetail,
				involvedPartyList, issueList, addlInfoList);

		List<TicketData> ticketDataList = new ArrayList<>();
		ticketDataList.add(ticketData);

		return new CreateTktReqData(ticketDataList);

	}*/

}
