package com.hra.api.stepdef;

import java.io.IOException;
import java.text.ParseException;

import com.hra.api.ticket.CreateTicket;
import com.hra.api.ticket.UpdateTicket;
import com.hra.api.token.TokenId;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ApiStepDef {
	
	CreateTicket createTicket = new CreateTicket();
	UpdateTicket updateTicket = new UpdateTicket();
	
	@Given("^token is generated using api$")
	public void token_is_generated_using_api() throws ParseException, IOException {
	    TokenId.getToken();
	}
	
	@Given("^user gets source ticket IDs$")
	public void user_gets_source_ticket_IDs() throws IOException {
	    createTicket.getSrcTktIds();
	}

	@When("^user calls api to create \"([^\"]*)\" tickets$")
	public void user_calls_api_to_create_tickets(String count) throws IOException, InterruptedException{
	    createTicket.createTktData(Integer.parseInt(count));
	    createTicket.createTktApiReq();
	}

	@Then("^verify create ticket request is submitted and reference id is generated$")
	public void verify_create_ticket_request_is_submitted_and_reference_id_is_generated() throws IOException {
	    createTicket.verifyRefIdForCreateTktReq();
	}
	
	@When("^user calls api to update ticket$")
	public void user_calls_api_to_update_ticket() throws IOException, InterruptedException {
	    updateTicket.updateTktData();
	    updateTicket.updateTktApiReq();
	}

	@Given("^user gets reference ID and source ticket IDs$")
	public void user_gets_reference_ID_and_source_ticket_IDs() throws IOException{
	    createTicket.getRefIdNSrcTktIds();
	}

	@When("^user calls api to get ticket id by reference id$")
	public void user_calls_api_to_get_ticket_id_by_reference_id() throws ParseException, IOException, InterruptedException  {
		createTicket.getTktIdByRefIdReq();
	}
	
	@When("^user calls api to get ticket ids by source ticket ids$")
	public void user_calls_api_to_get_ticket_ids_by_source_ticket_ids() throws IOException, InterruptedException, ParseException  {
		createTicket.getTktIdBySrcTktIdReq();
	}

	@Then("^verify tickets are created by reference id$")
	public void verify_tickets_are_created_by_reference_id() throws IOException {
	    createTicket.verifyTicketsCreatedByRefId();
	}
	
	@Then("^verify tickets are created by source ticket ids$")
	public void verify_tickets_are_created_by_source_ticket_ids() throws IOException {
	    createTicket.verifyTicketCreatedBySrcTktIds();
	}
	
	@Then("^verify update request is submitted and reference_id is generated$")
	public void verify_update_request_is_submitted_and_reference_id_is_generated() throws IOException {
	    updateTicket.verifyRefIdForUpdateTktReq();
	}
	
	@Given("^user gets reference id and ticket ID$")
	public void user_gets_reference_id_and_ticket_ID() throws IOException  {
		updateTicket.getRefIdNTktId();
	}
	
	@When("^user calls api to get status of ticket update by reference id$")
	public void user_calls_api_to_get_status_of_ticket_update_by_reference_id() throws ParseException, IOException, InterruptedException  {
		updateTicket.getUpdatedTktStatusByRefIdReq();
	}
	
	@Then("^verify ticket is updated successfully$")
	public void verify_ticket_is_updated_successfully() throws IOException {
	    updateTicket.verifyTicketUpdated();
	}


}
