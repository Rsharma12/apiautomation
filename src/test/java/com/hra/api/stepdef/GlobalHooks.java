package com.hra.api.stepdef;

import com.hra.helper.ReportingUtils;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class GlobalHooks {
	@Before
	public static void startTest(Scenario scenario) {
		ReportingUtils.test = ReportingUtils.report.startTest(scenario.getName());
	}
	
	@After
	public static void endTest() {
		ReportingUtils.report.endTest(ReportingUtils.test);

	}

}
