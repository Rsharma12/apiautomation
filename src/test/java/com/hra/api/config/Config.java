package com.hra.api.config;

import java.util.HashMap;
import java.util.Map;

public class Config {
	
	private Config(){
		
	}

	public static String[] notificationMethods = { "Manager Notified HR", "800 Intake Call", "Ethics Hotline", "EMAIL" };
//	public static String[] groups = { "Berlin", "abc", "QA Group", "Telemarketing", "Training" };
	public static String[] groups = { "Berlin", "QA Group", "Training" }; //Chenoa,HR
	public static String[] locations = { "Australia", "Albany", "Berlin", "Boston", "France" };
	public static String[] invPartyRoles = { "Complainant", "Subject", "Witness" };
	public static String[] issueCategories = { "Discrimination", "Harassment", "Recruitment" };
	public static String[] issueSubCategories = { "Age", "Bullying", "Recruitment" };
	
//	For SB testing
/*	public static final String CLIENT_SECRET = "7f5d1058902845b6bc74888424ecfb0679beea32e1e2442fbb5265ff2cfa46bf";
	public static final String BASE_URL = "https://api-sb.hracuity.net/v1/";
	public static final String CLIENT_ID = "qa-demo";*/
	
//	For Qa3 Demo
	public static final String CLIENT_SECRET = "c7cb8df7f077400bb3d11942db156e194d22c0755ba94cefa4e61ebc90bc2c30";
	public static final String BASE_URL = "https://qa3-openapi.hracuity.net/v1/";
	public static final String CLIENT_ID = "qa3-demo";
	
//	For QA Demo
/*	public static final String CLIENT_SECRET = "450f79d1b8644ecb83a57d1cada7fcd7b8213e53d85a42f5a665443006bb0901";
	public static final String BASE_URL = "https://qa-openapi.hracuity.net/v1/";
	public static final String CLIENT_ID = "qa-demo";*/
	
//	For HRADev 
	/*public static final String CLIENT_SECRET = "39041781c5104b99b2e18eed85cd4486830e295d9f7d4c92bcfc2c1d0e875da2";
	public static final String BASE_URL = "https://pushapi.hracuity.net/v1/";
	public static final String CLIENT_ID = "HRADev";*/
	
	public static final String CREATE_TKT_DATA_JSON_FILE = "src/test/resources/create_tkt_data.json";
	public static final String TKTID_REFID_DATA_JSON_FILE = "src/test/resources/tktid_refid_data.json";
	public static final String UPDATE_TKT_DATA_JSON_FILE = "src/test/resources/update_tkt_data.json";
	
	public static final String APP_JSON = "application/json; charset=UTF-8";
	public static final String APP_URLENCODED = "application/x-www-form-urlencoded";
	
	public static final Map<String,String> PARAM_MAP = new HashMap<>();
	
	static{
		PARAM_MAP.put("grant_type", "client_credentials");
		PARAM_MAP.put("client_id", CLIENT_ID);
		PARAM_MAP.put("client_secret", CLIENT_SECRET);
	}
	
}
