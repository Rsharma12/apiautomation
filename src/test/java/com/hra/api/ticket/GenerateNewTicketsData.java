package com.hra.api.ticket;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.hra.api.pojo.AddlInfo;
import com.hra.api.pojo.CreateTktReqData;
import com.hra.api.pojo.InvolvedParty;
import com.hra.api.pojo.Issue;
import com.hra.api.pojo.TicketData;

import static com.hra.api.config.Config.*;


public class GenerateNewTicketsData {
	
	public CreateTktReqData createTktData(int count) throws InterruptedException {
		
		List<TicketData> ticketDataList = new ArrayList<>();
		
		for(int i = 0; i < count; i++){
			TimeUnit.SECONDS.sleep(1);
			LocalDateTime now = LocalDateTime.now();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
			DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("HHmmss");
			String time = now.format(dtf1);
			String currentTime = now.format(dtf);
			System.out.println(currentTime);
			Random r = new Random();

			AddlInfo addlInfo = new AddlInfo("Additional Info", "Additional info at time - " + currentTime);
			List<AddlInfo> addlInfoList = new ArrayList<>();
			addlInfoList.add(addlInfo);

			int rand = r.nextInt(issueCategories.length);
			Issue issue = new Issue(issueCategories[rand], issueSubCategories[rand],
					"Issue description at time - " + currentTime);
			List<Issue> issueList = new ArrayList<>();
			issueList.add(issue);

			rand = r.nextInt(invPartyRoles.length);
			InvolvedParty involvedParty = new InvolvedParty("Test FN", "Test LN", "EID-1234", invPartyRoles[rand]);
			List<InvolvedParty> involvedPartyList = new ArrayList<>();
			involvedPartyList.add(involvedParty);

			String srcTicketId = "TKT-AUTO-API-" + time;
			
			String caseName = "Case-" + time;
			rand = r.nextInt(groups.length);
			String group = groups[rand];

			rand = r.nextInt(locations.length);
			String location = locations[rand];

			rand = r.nextInt(notificationMethods.length);
			String notificationMethod = notificationMethods[rand];

			String issueDetail = "Ticket created using Automated API at - " + currentTime;

			TicketData ticketData = new TicketData(srcTicketId, caseName, group, location, notificationMethod, issueDetail,
					involvedPartyList, issueList, addlInfoList);

			ticketDataList.add(ticketData);
		}
		
		return new CreateTktReqData(ticketDataList);

	}

}
