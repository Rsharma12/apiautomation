package com.hra.api.ticket;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.hra.api.pojo.AddlInfo;
import com.hra.api.pojo.CreateTktReqData;
import com.hra.api.pojo.GetTktIdByRefIdResponse;
import com.hra.api.pojo.GetTktIdByRefIdStatus;
import com.hra.api.pojo.InvolvedParty;
import com.hra.api.pojo.Issue;
import com.hra.api.pojo.TicketData;
import com.hra.api.pojo.UpdateTktByTktId;
import com.hra.api.pojo.UpdateTktByTktIdReqData;
import com.hra.helper.JsonUtil;

import static com.hra.api.config.Config.*;

public class GenerateUpdateTicketData {

	public UpdateTktByTktIdReqData updateTktData() throws InterruptedException, IOException {

		

		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
		DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("HHmmss");
		DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String time = now.format(dtf1);
		String currentTime = now.format(dtf);
		Random r = new Random();
		
		String notificationDate = now.format(dtf2);

		AddlInfo addlInfo = new AddlInfo("Additional Info", "Additional info updated at time - " + currentTime);
		List<AddlInfo> addlInfoList = new ArrayList<>();
		addlInfoList.add(addlInfo);

		int rand = r.nextInt(issueCategories.length);
		Issue issue = new Issue(issueCategories[rand], issueSubCategories[rand],
				"Issue description updated at time - " + currentTime);
		List<Issue> issueList = new ArrayList<>();
		issueList.add(issue);

		rand = r.nextInt(invPartyRoles.length);
		InvolvedParty involvedParty = new InvolvedParty("Test FN - Updated", "Test LN - Updated", "EID-1234", invPartyRoles[rand]);
		List<InvolvedParty> involvedPartyList = new ArrayList<>();
		involvedPartyList.add(involvedParty);
		
		CreateTktReqData createTktReqData = JsonUtil.readFromFile(CREATE_TKT_DATA_JSON_FILE, CreateTktReqData.class);
		
		TicketData td = createTktReqData.getData().get(0);
		
		String srcTicketId = td.getSrcTicketId();
		
		GetTktIdByRefIdResponse tktIdByRefIdResponse = JsonUtil.readFromFile(TKTID_REFID_DATA_JSON_FILE, GetTktIdByRefIdResponse.class);
		
		String ticketId = null;
		
		for(GetTktIdByRefIdStatus tktIdByRefIdStatus : tktIdByRefIdResponse.getData()){
			if(tktIdByRefIdStatus.getSrcTicketId().equals(srcTicketId)){
				ticketId = tktIdByRefIdStatus.getTicketNo();
				break;
			}
		}
		
		String caseName = "Case-Updated-" + time;
		do{
			rand = r.nextInt(groups.length);
		}while(td.getGroup().equals(groups[rand]));
		
		String group = groups[rand];

		do{
			rand = r.nextInt(locations.length);
		}while(td.getLocation().equals(locations[rand]));
		
		String location = locations[rand];

		do{
			rand = r.nextInt(notificationMethods.length);
		}while(td.getNotificationMethod().equals(notificationMethods[rand]));
		
		String notificationMethod = notificationMethods[rand];

		String issueDetail = "Ticket updated using Automated API at - " + currentTime;
		
		UpdateTktByTktId updateTktByTktId = new UpdateTktByTktId(ticketId, caseName, group, location, notificationMethod, notificationDate, issueDetail, involvedPartyList, issueList, addlInfoList);

		return new UpdateTktByTktIdReqData(updateTktByTktId);

	}

}
