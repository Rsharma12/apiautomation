package com.hra.api.ticket;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;

import com.hra.api.pojo.TicketResponse;
import com.hra.api.pojo.CreateTktReqData;
import com.hra.api.pojo.GetTktIdByRefIdResponse;
import com.hra.api.pojo.GetTktIdByRefIdStatus;
import com.hra.api.pojo.GetTktIdsBySrcTktIdsResponse;
import com.hra.api.pojo.SrcTicketId;
import com.hra.api.pojo.SrcTicketIdResponse;
import com.hra.api.pojo.TktIdByRefIdReqData;
import com.hra.api.pojo.TktIdBySrcTktIdReqData;
import com.hra.api.token.TokenId;
import com.hra.helper.JsonUtil;
import com.hra.helper.ReportingUtils;
import com.jayway.restassured.response.Response;
import com.relevantcodes.extentreports.LogStatus;

import static com.hra.api.config.Config.*;
import static com.jayway.restassured.RestAssured.*;

public class CreateTicket {

	CreateTktReqData createTktReqData;
	String path = "src/test/resources/properties/ticket_params_api.properties";

	Response response;
	String referenceId = null;
	List<String> srcTktIdList = new ArrayList<>();

	public void createTktData(int count) throws IOException, InterruptedException {
		GenerateNewTicketsData ticketData = new GenerateNewTicketsData();
		createTktReqData = ticketData.createTktData(count);
		JsonUtil.writeToFile(createTktReqData, CREATE_TKT_DATA_JSON_FILE);

	}

	public void createTktApiReq() throws IOException {
		
		String jsonStr = JsonUtil.objToJsonStr(createTktReqData);
	
		ReportingUtils.log(LogStatus.INFO, "Ticket data to be created - " + jsonStr);
		
		for(int i = 0;i < createTktReqData.getData().size();i++){
			ReportingUtils.log(LogStatus.INFO, "Source Ticket ID for ticket to be created is - " + createTktReqData.getData().get(i).getSrcTicketId());
		}

		String createTktReqUrl = BASE_URL + "CreateTicket";

		response = given().authentication().oauth2(TokenId.tokenId).contentType(APP_JSON)
				.body(jsonStr).when().post(createTktReqUrl);

		if (response.getStatusCode() == 200) {
			ReportingUtils.log(LogStatus.PASS, "Create Ticket Request submitted successfully");
		} else {
			ReportingUtils.log(LogStatus.FAIL, "Create Ticket Request failed. " + response.asString());
			Assert.fail("Create Ticket Request failed. " + response.asString());
		}

	}

	public void verifyRefIdForCreateTktReq() throws IOException {
		if (response.getStatusCode() == 200) {
			TicketResponse createTicketResponse = response.as(TicketResponse.class);
			referenceId = createTicketResponse.getData().getReferenceId();
			Properties props = new Properties();
			FileOutputStream out = new FileOutputStream(path);
			props.setProperty("reference_id", referenceId);
			props.setProperty("count", Integer.toString(createTktReqData.getData().size()));
			for (int i = 0; i < createTktReqData.getData().size(); i++) {
				props.setProperty("src_tkt_id" + i, createTktReqData.getData().get(i).getSrcTicketId());
			}
			props.store(out, null);
			out.close();
			ReportingUtils.log(LogStatus.PASS, "Create Ticket Request submitted successfully");
			ReportingUtils.log(LogStatus.INFO, "Reference ID generated is: " + referenceId);
		}

	}

	public void getRefIdNSrcTktIds() throws IOException {

		Properties props = new Properties();
		FileInputStream in = new FileInputStream(path);
		props.load(in);
		referenceId = props.getProperty("reference_id");
		int count = Integer.parseInt(props.getProperty("count"));
		srcTktIdList.clear();
		for (int i = 0; i < count; i++) {
			srcTktIdList.add(props.getProperty("src_tkt_id" + i));
		}
		in.close();

	}

	public void getTktIdByRefIdReq() throws ParseException, IOException, InterruptedException {
		
		TktIdByRefIdReqData tktIdByRefIdReqData = new TktIdByRefIdReqData(referenceId);
		
		String jsonStr = JsonUtil.objToJsonStr(tktIdByRefIdReqData);

		String getTicketIdByRefIdUrl = BASE_URL + "GetTicketIdByRefId";

		response = given().authentication().oauth2(TokenId.tokenId).contentType(APP_JSON)
				.body(jsonStr).when().post(getTicketIdByRefIdUrl);

		while (response.asString().contains("is in progress") || response.asString().contains("Wait and retry the operation")) {
			TimeUnit.SECONDS.sleep(15);
			if (!TokenId.isValid()) {
				TokenId.getToken();
			} 
			response = given().authentication().oauth2(TokenId.tokenId)
					.contentType(APP_JSON).body(jsonStr).when()
					.post(getTicketIdByRefIdUrl);

			System.out.println("Retrying ....");
		}

		ReportingUtils.log(LogStatus.PASS, "Get ticket ID by Reference ID request submitted successfully" + referenceId);

	}

	public void verifyTicketsCreatedByRefId() throws IOException{
		if (response.getStatusCode() == 200) {
			GetTktIdByRefIdResponse getTktIdByRefIdResponse = response.as(GetTktIdByRefIdResponse.class);
			JsonUtil.writeToFile(getTktIdByRefIdResponse, TKTID_REFID_DATA_JSON_FILE);
			Properties props = new Properties();
			FileInputStream in = new FileInputStream(path);
			props.load(in);
			referenceId = props.getProperty("reference_id");
			int count = Integer.parseInt(props.getProperty("count"));
			List<String> expSrcTktIdList = new ArrayList<>();
			for (int i = 0; i < count; i++) {
				expSrcTktIdList.add(props.getProperty("src_tkt_id" + i));
			}

			for (GetTktIdByRefIdStatus status : getTktIdByRefIdResponse.getData()) {
				if(expSrcTktIdList.contains(status.getSrcTicketId())){
					ReportingUtils.log(LogStatus.INFO, "Ticket ID generated is - " + status.getTicketNo() + " and Source Ticket ID is - " + status.getSrcTicketId());
				}else{
					ReportingUtils.log(LogStatus.FAIL, "Unexpected Source Ticket ID - " + status.getSrcTicketId());
					Assert.fail( "Unexpected Source Ticket ID - " + status.getSrcTicketId() + " in GetTktIdByRefId api request response");
				}
				
				
			}

			ReportingUtils.log(LogStatus.PASS, "Tickets are created successfully in HRA queue");
		}else{
			ReportingUtils.log(LogStatus.FAIL, "Tickets are not created in HRA queue. " + response.asString());
		}

	}

	public void getSrcTktIds() throws IOException {
		Properties props = new Properties();
		FileInputStream in = new FileInputStream(path);
		props.load(in);
		int count = Integer.parseInt(props.getProperty("count"));
		for(int i = 0; i < count ; i++){
			srcTktIdList.add(props.getProperty("src_tkt_id" + i));
		}
		
		ReportingUtils.log(LogStatus.PASS, "Source Ticket ID is - " + srcTktIdList);

	}
	
	public void getTktIdBySrcTktIdReq() throws IOException, InterruptedException, ParseException {
		TktIdBySrcTktIdReqData tktIdBySrcTktIdReqData = new TktIdBySrcTktIdReqData();
		for(String tkt : srcTktIdList){
			SrcTicketId srcTktId = new SrcTicketId();
			srcTktId.setSrcTicketId(tkt);
			tktIdBySrcTktIdReqData.getData().add(srcTktId);			
		}
		
		String jsonStr = JsonUtil.objToJsonStr(tktIdBySrcTktIdReqData);
		
		String getTicketIdsBySourceTicketIdsUrl = BASE_URL + "GetTicketIdsBySourceTicketIds";

		response = given().authentication().oauth2(TokenId.tokenId).contentType(APP_JSON)
				.body(jsonStr).when().post(getTicketIdsBySourceTicketIdsUrl);
		
		while (response.asString().contains("Ticket is not yet created")) {
			TimeUnit.SECONDS.sleep(15);
			if (!TokenId.isValid()) {
				TokenId.getToken();
			} 
			response = given().authentication().oauth2(TokenId.tokenId)
					.contentType(APP_JSON).body(jsonStr).when()
					.post(getTicketIdsBySourceTicketIdsUrl);

			System.out.println("Retrying ....");
		}
		
		ReportingUtils.log(LogStatus.PASS, "Get Ticket IDs by Source Ticket IDs Request submitted successfully. " + response.asString());
		
	}


	public void verifyTicketCreatedBySrcTktIds() throws IOException {
		if (response.getStatusCode() == 200) {
			GetTktIdsBySrcTktIdsResponse getTktIdsBySrcTktIdsResponse = response.as(GetTktIdsBySrcTktIdsResponse.class);
			
			for (SrcTicketIdResponse srcTicketIdResponse : getTktIdsBySrcTktIdsResponse.getData()) {
				if(srcTktIdList.contains(srcTicketIdResponse.getSrcTicketId())){
					ReportingUtils.log(LogStatus.INFO, "Ticket ID generated is - " + srcTicketIdResponse.getTicketNo() + " and Source Ticket ID is - " + srcTicketIdResponse.getSrcTicketId());
				}else{
					ReportingUtils.log(LogStatus.FAIL, "Unexpected Source Ticket ID - " + srcTicketIdResponse.getSrcTicketId() + " in response data.");
					Assert.fail( "Unexpected Source Ticket ID - " + srcTicketIdResponse.getSrcTicketId() + " in GetTktIdsBySrcTktIds api request response");
				}
				
				
			}

			ReportingUtils.log(LogStatus.PASS, "Tickets are created successfully in HRA queue");
		}else{
			ReportingUtils.log(LogStatus.FAIL, "Tickets are not created in HRA queue. " + response.asString());
		}
		
	}

	

}
