package com.hra.api.ticket;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.testng.Assert;

import com.hra.api.pojo.TicketResponse;
import com.hra.api.pojo.GetTktIdByRefIdResponse;
import com.hra.api.pojo.GetTktIdByRefIdStatus;
import com.hra.api.pojo.GetTktIdsBySrcTktIdsResponse;
import com.hra.api.pojo.SrcTicketId;
import com.hra.api.pojo.SrcTicketIdResponse;
import com.hra.api.pojo.TktIdByRefIdReqData;
import com.hra.api.pojo.TktIdBySrcTktIdReqData;
import com.hra.api.pojo.UpdateTktByTktIdReqData;
import com.hra.api.token.TokenId;
import com.hra.helper.JsonUtil;
import com.hra.helper.ReportingUtils;
import com.jayway.restassured.response.Response;
import com.relevantcodes.extentreports.LogStatus;

import static com.hra.api.config.Config.*;
import static com.jayway.restassured.RestAssured.*;

public class UpdateTicket {

	UpdateTktByTktIdReqData updateTktByTktIdReqData;
	String path = "src/test/resources/properties/ticket_params_api.properties";

	Response response;
	String referenceId = null;
	List<String> srcTktIdList = new ArrayList<>();
	String tktId = null;

	public void updateTktData() throws IOException, InterruptedException {
		GenerateUpdateTicketData ticketData = new GenerateUpdateTicketData();
		updateTktByTktIdReqData = ticketData.updateTktData();
		JsonUtil.writeToFile(updateTktByTktIdReqData, UPDATE_TKT_DATA_JSON_FILE);

	}

	public void updateTktApiReq() throws IOException {

		String jsonStr = JsonUtil.objToJsonStr(updateTktByTktIdReqData);

		ReportingUtils.log(LogStatus.INFO, "Ticket data to be updated - " + jsonStr);
		
		String updateTicketByTicketIdReqUrl = BASE_URL + "UpdateTicketByTicketId";

		response = given().authentication().oauth2(TokenId.tokenId).contentType(APP_JSON)
				.body(jsonStr).when().post(updateTicketByTicketIdReqUrl);

		if (response.getStatusCode() == 200) {
			ReportingUtils.log(LogStatus.PASS, "Update Ticket Request submitted successfully");
		} else {
			ReportingUtils.log(LogStatus.FAIL, "Update Ticket Request failed. " + response.asString());
			Assert.fail("Update Ticket Request failed. " + response.asString());
		}

	}

	public void verifyRefIdForUpdateTktReq() throws IOException {
		if (response.getStatusCode() == 200) {
			TicketResponse updateTicketResponse = response.as(TicketResponse.class);
			referenceId = updateTicketResponse.getData().getReferenceId();
			tktId = updateTktByTktIdReqData.getData().getTicketId();
			Properties props = new Properties();
			FileInputStream in = new FileInputStream(path);
			props.load(in);
			in.close();
			FileOutputStream out = new FileOutputStream(path);
			props.setProperty("update_tkt_reference_id", referenceId);
			props.setProperty("update_tkt_id", tktId);
			props.store(out, null);
			out.close();
			ReportingUtils.log(LogStatus.PASS, "Reference ID generated for update ticket request is: " + referenceId);
		}

	}

	public void getRefIdNTktId() throws IOException {

		Properties props = new Properties();
		FileInputStream in = new FileInputStream(path);
		props.load(in);
		referenceId = props.getProperty("update_tkt_reference_id");
		tktId = props.getProperty("update_tkt_id");
		in.close();

	}

	public void getUpdatedTktStatusByRefIdReq() throws ParseException, IOException, InterruptedException {
		
		TktIdByRefIdReqData tktIdByRefIdReqData = new TktIdByRefIdReqData(referenceId);

		String jsonStr = JsonUtil.objToJsonStr(tktIdByRefIdReqData);

		String getUpdatedTicketStatusByRefIdUrl = BASE_URL + "GetUpdatedTicketStatusByRefId";

		response = given().authentication().oauth2(TokenId.tokenId).contentType(APP_JSON)
				.body(jsonStr).when().post(getUpdatedTicketStatusByRefIdUrl);

		while (response.asString().contains("is in progress") || response.asString().contains("Wait and retry the operation")) {
			TimeUnit.SECONDS.sleep(15);
			if (!TokenId.isValid()) {
				TokenId.getToken();
			} 
			response = given().authentication().oauth2(TokenId.tokenId)
					.contentType(APP_JSON).body(jsonStr).when()
					.post(getUpdatedTicketStatusByRefIdUrl);

			System.out.println("Retrying ....");
		}

		ReportingUtils.log(LogStatus.PASS, "Get Updated ticket status by Reference ID request submitted successfully");

	}

	public void verifyTicketUpdated() throws IOException {
		if (response.getStatusCode() == 200) {
			GetTktIdByRefIdResponse getTktIdByRefIdResponse = response.as(GetTktIdByRefIdResponse.class);
			Properties props = new Properties();
			FileInputStream in = new FileInputStream(path);
			props.load(in);
			referenceId = props.getProperty("update_tkt_reference_id");
			String expTktId = props.getProperty("update_tkt_id");
			
			for (GetTktIdByRefIdStatus status : getTktIdByRefIdResponse.getData()) {
				if(expTktId.equals(status.getTicketNo())){
					ReportingUtils.log(LogStatus.INFO, "Expected Ticket ID updated is - " + expTktId + " and actual Ticket ID updated is - " + status.getTicketNo());
				}else{
					ReportingUtils.log(LogStatus.FAIL, "Expected Ticket ID updated is - " + expTktId + " and actual Ticket ID updated is - " + status.getTicketNo());
					Assert.fail("Expected Ticket ID updated is - " + expTktId + " and actual Ticket ID updated is - " + status.getTicketNo());
				}
				
				
			}

			ReportingUtils.log(LogStatus.PASS, "Ticket is updated successfully in HRA queue. " + response.asString());
		}else{
			ReportingUtils.log(LogStatus.FAIL, "Ticket is not updated in HRA queue. " + response.asString());
		}

	}

	public void getSrcTktIds() throws IOException {
		Properties props = new Properties();
		FileInputStream in = new FileInputStream(path);
		props.load(in);
		int count = Integer.parseInt(props.getProperty("count"));
		for(int i = 0; i < count ; i++){
			srcTktIdList.add(props.getProperty("src_tkt_id" + i));
		}
		
		ReportingUtils.log(LogStatus.PASS, "Source Ticket ID is - " + srcTktIdList);

	}
	
	public void getTktIdBySrcTktIdReq() throws IOException, InterruptedException, ParseException {
		TktIdBySrcTktIdReqData tktIdBySrcTktIdReqData = new TktIdBySrcTktIdReqData();
		for(String tkt : srcTktIdList){
			SrcTicketId srcTktId = new SrcTicketId();
			srcTktId.setSrcTicketId(tkt);
			tktIdBySrcTktIdReqData.getData().add(srcTktId);			
		}
		
		String jsonStr = JsonUtil.objToJsonStr(tktIdBySrcTktIdReqData);

		String getTicketIdsBySourceTicketIdsUrl = BASE_URL + "GetTicketIdsBySourceTicketIds";

		response = given().authentication().oauth2(TokenId.tokenId).contentType(APP_JSON)
				.body(jsonStr).when().post(getTicketIdsBySourceTicketIdsUrl);
		
		while (response.asString().contains("Ticket is not yet created")) {
			TimeUnit.SECONDS.sleep(15);
			if (!TokenId.isValid()) {
				TokenId.getToken();
			} 
			response = given().authentication().oauth2(TokenId.tokenId)
					.contentType(APP_JSON).body(jsonStr).when()
					.post(getTicketIdsBySourceTicketIdsUrl);

			System.out.println("Retrying ....");
		}
		
		ReportingUtils.log(LogStatus.PASS, "Get Ticket IDs by Source Ticket IDs Request submitted successfully. " + response.asString());
		
	}


	public void verifyTicketCreatedBySrcTktIds() throws IOException {
		if (response.getStatusCode() == 200) {
			GetTktIdsBySrcTktIdsResponse getTktIdsBySrcTktIdsResponse = response.as(GetTktIdsBySrcTktIdsResponse.class);
			
			for (SrcTicketIdResponse srcTicketIdResponse : getTktIdsBySrcTktIdsResponse.getData()) {
				if(srcTktIdList.contains(srcTicketIdResponse.getSrcTicketId())){
					ReportingUtils.log(LogStatus.INFO, "Ticket ID generated is - " + srcTicketIdResponse.getTicketNo() + " and Source Ticket ID is - " + srcTicketIdResponse.getSrcTicketId());
				}else{
					ReportingUtils.log(LogStatus.FAIL, "Unexpected Source Ticket ID - " + srcTicketIdResponse.getSrcTicketId() + " in response data.");
					Assert.fail( "Unexpected Source Ticket ID - " + srcTicketIdResponse.getSrcTicketId() + " in GetTktIdsBySrcTktIds api request response");
				}
				
				
			}

			ReportingUtils.log(LogStatus.PASS, "Tickets are created successfully in HRA queue. " + response.asString());
		}else{
			ReportingUtils.log(LogStatus.FAIL, "Tickets are not created in HRA queue. " + response.asString());
		}
		
	}

	

}
