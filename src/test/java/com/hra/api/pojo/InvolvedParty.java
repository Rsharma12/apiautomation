package com.hra.api.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InvolvedParty {
	
	@JsonProperty("first_name")
	private String firstName;
	@JsonProperty("last_name")
	private String lastName;
	@JsonProperty("employee_id")
	private String employeeId;
	private String type;
	
	public InvolvedParty(){
		
	}
	
	public InvolvedParty(String firstName, String lastName, String employeeId, String type) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.employeeId = employeeId;
		this.type = type;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "InvolvedParty [firstName=" + firstName + ", lastName=" + lastName + ", employeeId=" + employeeId
				+ ", type=" + type + "]";
	}
	
	

}
