package com.hra.api.pojo;

import java.util.ArrayList;
import java.util.List;

public class TktIdBySrcTktIdReqData {
	List<SrcTicketId> data = new ArrayList<>();

	public List<SrcTicketId> getData() {
		return data;
	}

	public void setData(List<SrcTicketId> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "TktIdBySrcTktIdReqData [data=" + data + "]";
	}
	
	

}
