package com.hra.api.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SrcTicketId {
	@JsonProperty("src_ticket_id")
	private String srcTicketId;

	public String getSrcTicketId() {
		return srcTicketId;
	}

	public void setSrcTicketId(String srcTicketId) {
		this.srcTicketId = srcTicketId;
	}

	@Override
	public String toString() {
		return "SrcTicketId [srcTicketId=" + srcTicketId + "]";
	}
	
	

}
