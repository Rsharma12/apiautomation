package com.hra.api.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TicketData {
	
	@JsonProperty("src_ticket_id")
	private String srcTicketId;
	@JsonProperty("case_name")
	private String caseName;
	private String group;
	private String location;
	@JsonProperty("notification_method")
	private String notificationMethod;
	@JsonProperty("issue_detail")
	private String issueDetail;	
	@JsonProperty("involved_party")
	private List<InvolvedParty> involvedParty;	
	private List<Issue> issues;
	@JsonProperty("addl_info")
	private List<AddlInfo> addlInfo;
	
	public TicketData(){
		
	}

	public TicketData(String srcTicketId, String caseName, String group, String location, String notificationMethod,
			String issueDetail, List<InvolvedParty> involvedParty, List<Issue> issues, List<AddlInfo> addlInfo) {
		this.srcTicketId = srcTicketId;
		this.caseName = caseName;
		this.group = group;
		this.location = location;
		this.notificationMethod = notificationMethod;
		this.issueDetail = issueDetail;
		this.involvedParty = involvedParty;
		this.issues = issues;
		this.addlInfo = addlInfo;
	}

	public String getSrcTicketId() {
		return srcTicketId;
	}

	public void setSrcTicketId(String srcTicketId) {
		this.srcTicketId = srcTicketId;
	}

	public String getCaseName() {
		return caseName;
	}

	public void setCaseName(String caseName) {
		this.caseName = caseName;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getNotificationMethod() {
		return notificationMethod;
	}

	public void setNotificationMethod(String notificationMethod) {
		this.notificationMethod = notificationMethod;
	}

	public String getIssueDetail() {
		return issueDetail;
	}

	public void setIssueDetail(String issueDetail) {
		this.issueDetail = issueDetail;
	}

	public List<InvolvedParty> getInvolvedParty() {
		return involvedParty;
	}

	public void setInvolvedParty(List<InvolvedParty> involvedParty) {
		this.involvedParty = involvedParty;
	}

	public List<Issue> getIssues() {
		return issues;
	}

	public void setIssues(List<Issue> issues) {
		this.issues = issues;
	}

	public List<AddlInfo> getAddlInfo() {
		return addlInfo;
	}

	public void setAddlInfo(List<AddlInfo> addlInfo) {
		this.addlInfo = addlInfo;
	}

	@Override
	public String toString() {
		return "TicketData [srcTicketId=" + srcTicketId + ", caseName=" + caseName + ", group=" + group + ", location="
				+ location + ", notificationMethod=" + notificationMethod + ", issueDetail=" + issueDetail
				+ ", involvedParty=" + involvedParty + ", issues=" + issues + ", addlInfo=" + addlInfo + "]";
	}

}
