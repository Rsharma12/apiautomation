package com.hra.api.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetTktIdByRefIdResponse {
	private int status;
	@JsonProperty("hra_trace_id")
	private String hraTraceId;
	private List<GetTktIdByRefIdStatus> data;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getHraTraceId() {
		return hraTraceId;
	}
	public void setHraTraceId(String hraTraceId) {
		this.hraTraceId = hraTraceId;
	}
	public List<GetTktIdByRefIdStatus> getData() {
		return data;
	}
	public void setData(List<GetTktIdByRefIdStatus> data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "GetTktIdByRefIdResponse [status=" + status + ", hraTraceId=" + hraTraceId + ", data=" + data + "]";
	}
	
}
