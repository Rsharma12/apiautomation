package com.hra.api.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateTktByTktId {
	
	@JsonProperty("ticket_id") private String ticketId;
	@JsonProperty("case_name") private String caseName;
	private String group;
	private String location;
	@JsonProperty("notification_method") private String notificationMethod;
	@JsonProperty("notification_date") private String notificationDate;
	@JsonProperty("issue_detail") private String issueDetail;
	@JsonProperty("involved_party") private List<InvolvedParty> invPartyList;
	@JsonProperty("issues") private List<Issue> issuesList;
	@JsonProperty("addl_info") private List<AddlInfo> addlInfoList;
	
	public UpdateTktByTktId(){
		
	}
	public UpdateTktByTktId(String ticketId, String caseName, String group, String location, String notificationMethod,
			String notificationDate, String issueDetail, List<InvolvedParty> invPartyList, List<Issue> issuesList,
			List<AddlInfo> addlInfoList) {
		this.ticketId = ticketId;
		this.caseName = caseName;
		this.group = group;
		this.location = location;
		this.notificationMethod = notificationMethod;
		this.notificationDate = notificationDate;
		this.issueDetail = issueDetail;
		this.invPartyList = invPartyList;
		this.issuesList = issuesList;
		this.addlInfoList = addlInfoList;
	}
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	public String getCaseName() {
		return caseName;
	}
	public void setCaseName(String caseName) {
		this.caseName = caseName;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getNotificationMethod() {
		return notificationMethod;
	}
	public void setNotificationMethod(String notificationMethod) {
		this.notificationMethod = notificationMethod;
	}
	public String getNotificationDate() {
		return notificationDate;
	}
	public void setNotificationDate(String notificationDate) {
		this.notificationDate = notificationDate;
	}
	public String getIssueDetail() {
		return issueDetail;
	}
	public void setIssueDetail(String issueDetail) {
		this.issueDetail = issueDetail;
	}
	public List<InvolvedParty> getInvPartyList() {
		return invPartyList;
	}
	public void setInvPartyList(List<InvolvedParty> invPartyList) {
		this.invPartyList = invPartyList;
	}
	public List<Issue> getIssuesList() {
		return issuesList;
	}
	public void setIssuesList(List<Issue> issuesList) {
		this.issuesList = issuesList;
	}
	public List<AddlInfo> getAddlInfoList() {
		return addlInfoList;
	}
	public void setAddlInfoList(List<AddlInfo> addlInfoList) {
		this.addlInfoList = addlInfoList;
	}
	@Override
	public String toString() {
		return "UpdateTktIdByTktId [ticketId=" + ticketId + ", caseName=" + caseName + ", group=" + group
				+ ", location=" + location + ", notificationMethod=" + notificationMethod + ", notificationDate="
				+ notificationDate + ", issueDetail=" + issueDetail + ", invPartyList=" + invPartyList + ", issuesList="
				+ issuesList + ", addlInfoList=" + addlInfoList + "]";
	}
	
	
	

}




