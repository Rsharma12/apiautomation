package com.hra.api.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TicketResponse {
	
	private int status;
	@JsonProperty("hra_trace_id")
	private String hraTraceId;
	ReferenceId data;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getHraTraceId() {
		return hraTraceId;
	}
	public void setHraTraceId(String hraTraceId) {
		this.hraTraceId = hraTraceId;
	}
	public ReferenceId getData() {
		return data;
	}
	public void setData(ReferenceId data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "CreateTicketResponse [status=" + status + ", hraTraceId=" + hraTraceId + ", data=" + data + "]";
	}
	
	

}
