package com.hra.api.pojo;

public class TktIdByRefIdReqData {
	private ReferenceId data;

	public TktIdByRefIdReqData(String referenceId) {
		this.data = new ReferenceId(referenceId);
	}

	public ReferenceId getData() {
		return data;
	}

	public void setData(ReferenceId data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "TktIdByRefIdReq [data=" + data + "]";
	}
	

}
