package com.hra.api.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SrcTicketIdResponse {
		private int status;
		
		@JsonProperty("src_ticket_id")
		private String srcTicketId;
		
		@JsonProperty("ticket_no")
		private String ticketNo;
		
		private String error;
		private String message;
		public int getStatus() {
			return status;
		}
		public void setStatus(int status) {
			this.status = status;
		}
		public String getSrcTicketId() {
			return srcTicketId;
		}
		public void setSrcTicketId(String srcTicketId) {
			this.srcTicketId = srcTicketId;
		}
		public String getTicketNo() {
			return ticketNo;
		}
		public void setTicketNo(String ticketNo) {
			this.ticketNo = ticketNo;
		}
		public String getError() {
			return error;
		}
		public void setError(String error) {
			this.error = error;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		@Override
		public String toString() {
			return "SrcTicketIdResponse [status=" + status + ", srcTicketId=" + srcTicketId + ", ticketNo=" + ticketNo
					+ ", error=" + error + ", message=" + message + "]";
		}
		
		
	
}
