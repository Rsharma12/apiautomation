package com.hra.api.pojo;

import java.util.List;

public class CaseResponse {
	
	private int status;
	private List<Case> cases;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public List<Case> getCases() {
		return cases;
	}
	public void setCases(List<Case> cases) {
		this.cases = cases;
	}
	@Override
	public String toString() {
		return "CaseResponse [status=" + status + ", cases=" + cases + "]";
	}
	
	

}
