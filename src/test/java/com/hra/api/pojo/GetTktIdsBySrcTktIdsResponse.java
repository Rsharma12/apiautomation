package com.hra.api.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetTktIdsBySrcTktIdsResponse {
	private int status;
	
	@JsonProperty("is_request_processed")
	private String isReqProcessed;
	
	@JsonProperty("hra_trace_id")
	private String hraTraceId;
	
	 private List<SrcTicketIdResponse> data;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getIsReqProcessed() {
		return isReqProcessed;
	}

	public void setIsReqProcessed(String isReqProcessed) {
		this.isReqProcessed = isReqProcessed;
	}

	public String getHraTraceId() {
		return hraTraceId;
	}

	public void setHraTraceId(String hraTraceId) {
		this.hraTraceId = hraTraceId;
	}

	public List<SrcTicketIdResponse> getData() {
		return data;
	}

	public void setData(List<SrcTicketIdResponse> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "TktIdBySrcTktIdResData [status=" + status + ", isReqProcessed=" + isReqProcessed + ", hraTraceId="
				+ hraTraceId + ", data=" + data + "]";
	}

	 
	
}
