package com.hra.api.pojo;

public class UpdateTktByTktIdReqData {
	
	UpdateTktByTktId data = new UpdateTktByTktId();

	public UpdateTktByTktIdReqData(UpdateTktByTktId updateTktByTktId) {
		this.data = updateTktByTktId;
	}
	
	public UpdateTktByTktIdReqData(){
		
	}

	public UpdateTktByTktId getData() {
		return data;
	}

	public void setData(UpdateTktByTktId data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "UpdateTktByTktIdReqData [data=" + data + "]";
	}
	
	

}
