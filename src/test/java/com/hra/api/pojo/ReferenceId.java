package com.hra.api.pojo;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ReferenceId {
	
	@JsonProperty("reference_id")
	@JsonAlias("reference_ID")
	private String referenceId;
	
	public ReferenceId(){
		
	}

	public ReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	@Override
	public String toString() {
		return "ReferenceId [referenceId=" + referenceId + "]";
	}
	

}
