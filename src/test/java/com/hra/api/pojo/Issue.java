package com.hra.api.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Issue {
	
	@JsonProperty("issue_category")
	private String issueCategory;
	@JsonProperty("issue_subcategory")
	private String issueSubCategory;
	@JsonProperty("issue_description")
	private String issueDescription;
	
	public Issue(){
		
	}
	
	
	public Issue(String issueCategory, String issueSubCategory, String issueDescription) {
		this.issueCategory = issueCategory;
		this.issueSubCategory = issueSubCategory;
		this.issueDescription = issueDescription;
	}
	public String getIssueCategory() {
		return issueCategory;
	}
	public void setIssueCategory(String issueCategory) {
		this.issueCategory = issueCategory;
	}
	public String getIssueSubCategory() {
		return issueSubCategory;
	}
	public void setIssueSubCategory(String issueSubCategory) {
		this.issueSubCategory = issueSubCategory;
	}
	public String getIssueDescription() {
		return issueDescription;
	}
	public void setIssueDescription(String issueDescription) {
		this.issueDescription = issueDescription;
	}
	@Override
	public String toString() {
		return "Issue [issueCategory=" + issueCategory + ", issueSubCategory=" + issueSubCategory
				+ ", issueDescription=" + issueDescription + "]";
	}
	

}
