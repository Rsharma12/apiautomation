package com.hra.api.pojo;

import java.util.List;

public class CreateTktReqData {
	
	List<TicketData> data;

	public List<TicketData> getData() {
		return data;
	}

	public void setData(List<TicketData> data) {
		this.data = data;
	}
	
	public CreateTktReqData() {

	}
	

	public CreateTktReqData(List<TicketData> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "CreateTktReqData [data=" + data + "]";
	}
	

}
