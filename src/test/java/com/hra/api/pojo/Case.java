package com.hra.api.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Case {
	
	private String status;
	@JsonProperty("case_id")
	private String caseId;
	@JsonProperty("case_status")
	private String caseStatus;
	private String error;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCaseId() {
		return caseId;
	}
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	public String getCaseStatus() {
		return caseStatus;
	}
	public void setCaseStatus(String caseStatus) {
		this.caseStatus = caseStatus;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	@Override
	public String toString() {
		return "Case [status=" + status + ", caseId=" + caseId + ", caseStatus=" + caseStatus + ", error=" + error
				+ "]";
	}
	
	

}
