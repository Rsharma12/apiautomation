package com.hra.api.pojo;

public class AddlInfo {
	
	private String key;
	private String value;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	public AddlInfo(){
		
	}
	
	public AddlInfo(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	@Override
	public String toString() {
		return "AddlInfo [key=" + key + ", value=" + value + "]";
	}

}
