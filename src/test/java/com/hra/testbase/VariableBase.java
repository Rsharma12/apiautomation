package com.hra.testbase;

public class VariableBase {

	public static String caseIdForSearch = "";

	public static void setCaseIdForSearch(String caseId) {
		caseIdForSearch = caseId;
	}

	public static String getCaseIdForSearch() {
		return caseIdForSearch;
	}
	
}
