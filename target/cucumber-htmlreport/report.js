$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/FeatureFiles/UpdatedTicketAPIs.feature");
formatter.feature({
  "line": 2,
  "name": "API for Updating tickets",
  "description": "",
  "id": "api-for-updating-tickets",
  "keyword": "Feature"
});
formatter.before({
  "duration": 2326217,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "update ticket using API",
  "description": "",
  "id": "api-for-updating-tickets;update-ticket-using-api",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@update"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "token is generated using api",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user calls api to update ticket",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "verify update request is submitted and reference_id is generated",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiStepDef.token_is_generated_using_api()"
});
formatter.result({
  "duration": 2897902103,
  "status": "passed"
});
formatter.match({
  "location": "ApiStepDef.user_calls_api_to_update_ticket()"
});
formatter.result({
  "duration": 491414295,
  "status": "passed"
});
formatter.match({
  "location": "ApiStepDef.verify_update_request_is_submitted_and_reference_id_is_generated()"
});
formatter.result({
  "duration": 5836523,
  "status": "passed"
});
formatter.after({
  "duration": 1571616,
  "status": "passed"
});
formatter.before({
  "duration": 67350,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "API to check status of updated ticket",
  "description": "",
  "id": "api-for-updating-tickets;api-to-check-status-of-updated-ticket",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 9,
      "name": "@CheckUpdateStatus"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "token is generated using api",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "user gets reference id and ticket ID",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user calls api to get status of ticket update by reference id",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "verify ticket is updated successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "ApiStepDef.token_is_generated_using_api()"
});
formatter.result({
  "duration": 822519910,
  "status": "passed"
});
formatter.match({
  "location": "ApiStepDef.user_gets_reference_id_and_ticket_ID()"
});
formatter.result({
  "duration": 335339,
  "status": "passed"
});
formatter.match({
  "location": "ApiStepDef.user_calls_api_to_get_status_of_ticket_update_by_reference_id()"
});
formatter.result({
  "duration": 217644468579,
  "status": "passed"
});
formatter.match({
  "location": "ApiStepDef.verify_ticket_is_updated_successfully()"
});
formatter.result({
  "duration": 7365119,
  "status": "passed"
});
formatter.after({
  "duration": 200287,
  "status": "passed"
});
});