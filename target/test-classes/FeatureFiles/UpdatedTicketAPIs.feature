
Feature: API for Updating tickets
  @update 
  Scenario: update ticket using API
    Given token is generated using api
    When user calls api to update ticket
    Then verify update request is submitted and reference_id is generated

  @CheckUpdateStatus
  Scenario: API to check status of updated ticket
   Given token is generated using api
   And user gets reference id and ticket ID
   When user calls api to get status of ticket update by reference id
   Then verify ticket is updated successfully