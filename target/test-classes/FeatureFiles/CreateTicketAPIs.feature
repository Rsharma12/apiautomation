Feature: APIs for creating tickets
  
  Scenario Outline: Create Ticket using API
    Given token is generated using api
    When user calls api to create "<count>" tickets
    Then verify create ticket request is submitted and reference id is generated 
  Examples:
  |count|
  |2|
    
  Scenario: Get Ticket ID by Reference ID
    Given token is generated using api
    And user gets reference ID and source ticket IDs
    When user calls api to get ticket id by reference id
    Then verify tickets are created by reference id
    
   Scenario: Get Ticket ID by Source Ticket ID
    Given token is generated using api
    And user gets source ticket IDs
    When user calls api to get ticket ids by source ticket ids
    Then verify tickets are created by source ticket ids